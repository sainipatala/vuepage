var pastgigs = [
  {
    event_name: "Syntikoiden Yö",
    venue_name: "Soundtools Oy",
    event_date: "27.10.2018",
    city: "Helsinki",
    fb_url: "https://www.facebook.com/events/356605261744182/",
    country: "Finland"
  },
  {
    event_name: "Ethereal Elektro at Tiksi Block Party",
    venue_name: "Tikkurila",
    event_date: "28.07.2018",
    city: "Vantaa",
    fb_url: "https://www.facebook.com/events/254901731769099/",
    country: "Finland"
  },
  {
    event_name: "Kotimaan Teknokatsaus",
    venue_name: "Ääniwalli",
    event_date: "16.03.2018",
    city: "Helsinki",
    fb_url: "https://www.facebook.com/events/107320253423905/",
    country: "Finland"
  },
]

var upcominggigs = [

]
