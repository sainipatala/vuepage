var navbar = Vue.component("Navbar", {
  template: `

  <nav id="mynav" class="navbar navbar-expand-lg">
    <img alt="logo" src="images/logos/se_3.png" class="logonavbar navbar-brand"></img>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li>
          <a class="nav-link" data-toggle="collapse" data-target=".navbar-collapse.show"><router-link to="/">SAINI ELISABET</router-link></a>
        </li>
        <li>
          <a class="nav-link" data-toggle="collapse" data-target=".navbar-collapse.show"><router-link to="/gigs">GIGS</router-link></a>
        </li>
        <li>
          <a class="nav-link" data-toggle="collapse" data-target=".navbar-collapse.show"><router-link to="/photos">PHOTOS</router-link></a>
        </li>
        <li>
          <a class="nav-link" data-toggle="collapse" data-target=".navbar-collapse.show"><router-link to="/contact">CONTACT</router-link></a>
        </li>
      </ul>
    </div>
  </nav>


`
});
