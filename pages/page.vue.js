var page = Vue.component("Page", {
  template: `
    <div id="page">
     <div class="pagecontainer">
        <router-view></router-view>
      </div>
    </div>

  `
});
