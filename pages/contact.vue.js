var contact = Vue.component("Contact", {
  template: `
    <div id="contact" class="yellow">
      <h4>CONTACT INFORMATION</h4>
      <p>For booking and other enquiries, please contact me via email or send me a message on social media.</p>
      <h4>EMAIL:</h4>
      <p>info@sainielisabet.com</p>
      <h4>SOCIAL MEDIA:</h4>
      <a href="https://www.facebook.com/saini.elisabet/" class="fa fa-facebook fa-2x" target="_blank"></a>
      <a href="https://www.youtube.com/c/saini_elisabet" class="fa fa-youtube fa-2x" target="_blank"></a>
      <a href="https://www.instagram.com/saini.elisabet/" class="fa fa-instagram fa-2x" target="_blank"></a>
      <a href="https://soundcloud.com/saini_elisabet" class="fa fa-soundcloud fa-2x" target="_blank"></a>
    </div>

`
});
