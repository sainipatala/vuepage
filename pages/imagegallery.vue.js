var imagegallery = Vue.component("Imagegallery", {
  template: `

    <div>
      <div v-for="collection in organizedimages">
        <imagegalleryrow :images="collection"></imagegalleryrow>
      </div>
    </div>


  `,
  data() {
    return {
      galleryimages: galleryimages,
    };
  },
  computed: {
    organizedimages: function () {
      let images = [];
      let rowimages = []
      for (index in this.galleryimages) {
        rowimages.push(this.galleryimages[index]);
        if(rowimages.length >= 3 || index >= this.galleryimages.length - 1) {
          images.push(rowimages);
          rowimages = [];
        }
      }
      return images;
    }
  }
});
