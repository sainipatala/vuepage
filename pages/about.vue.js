var about = Vue.component("About", {
  template: `
    <div id="about">
      <h4>SAINI ELISABET</h4>

      <p>Saini Elisabet is a Finnish techno producer and live performer. The main inspiration for her music
      comes from the 90's techno.
      She enjoys repetitive, monotonous and hypnotic beats as well as more atmospheric and melodic
      songs. 
      </p>
      <img alt="logo" src="images/logos/se_3.png" class="logo"></img>
    </div>

    `,
  data() {
    return {
    }

  }
});
