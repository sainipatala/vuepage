var imagegalleryrow = Vue.component("Imagegalleryrow", {
  template: `

    <div class="row">
      <div v-for="image in images" class="col-sm-4">
        <imagethumbnail :imageurl="image"></imagethumbnail>
      </div>
    </div>


  `,
  props: ['images'],
});
