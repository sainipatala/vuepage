Vue.use(VueRouter);

const routes = [
  {
        path: '/',
        component: home
  },
  {
        path: '/gigs',
        component: gigs
  },
  {
        path: '/about',
        component: about
  },
  {
        path: '/contact',
        component: contact
  },
  {
        path: '/photos',
        component: photos
  },
]

let router = new VueRouter({
  routes,
})

router.beforeEach((to, from, next) => {
    next()
})

var app = new Vue({
    el: '#app',
    watch: {},
    mounted() {

    },
    data: {
        msg: 'Hello',
        email: ''
    },
    methods: {},
    router
})

function getPageMaxHeigth() {
  var navHeight = document.getElementById('mynav').clientHeight;
  var footerHeight = document.getElementById('myfooter').clientHeight;
  var windowHeight = window.innerHeight;
  var pageMaxHeigth = windowHeight - navHeight - footerHeight;
  return pageMaxHeigth;
}
