Vue.component("imagethumbnail", {
  template: `
    <div class="imageborder">
      <img v-bind:src="imageurl" alt="gallery image" class="img-thumbnail grayscaleimg">
    </div>
  `,
  props: ['imageurl']
});
