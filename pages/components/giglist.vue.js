Vue.component("giglist", {
  template: `
  <ul>
    <li v-for="gig in gigsList" class="highline">
      <a class="normal-link" :href="gig.fb_url" target="_blank">
        <div class="row">
          <div class="col">
            {{ gig.event_date }}
          </div>
          <div class="col">
            {{ gig.event_name }}
          </div>
          <div class="col">
            {{ gig.venue_name }}
          </div>
          <div class="col">
            {{ gig.city }}
          </div>
          <div class="col">
            {{ gig.country }}
          </div>
        </div>
      </a>
    </li>
  </ul>
  `,
  props: ['gigsList']
});
