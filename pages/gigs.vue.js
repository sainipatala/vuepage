var gigs = Vue.component("Gigs", {
  template: `
    <div id="gigs">
      <h4> UPCOMING GIGS: </h4>
      <div v-if="upcominggigs.length > 0">
        <giglist :gigsList="upcominggigs"></giglist>
      </div>
      <div v-if="upcominggigs.length <= 0">
        <p>No upcoming gigs at the moment</p>
      </div>
      <img src="images/logos/triangle.png" alt="triangle" class="triangle">
      <h4> PAST GIGS: </h4>
      <giglist :gigsList="pastgigs"></giglist>
    </div>
  `,
  data() {
    return {
      pastgigs,
      upcominggigs
    };
  }
});
