var myfooter = Vue.component("Myfooter", {
  template: `

  <footer id="myfooter" class="page-footer fixed-bottom font-small">
    <div class="container-fluid text-center grey">
    <a href="https://www.facebook.com/saini.elisabet/" class="fa fa-facebook fa-1x" target="_blank"></a>
    <a href="https://www.youtube.com/c/saini_elisabet" class="fa fa-youtube fa-1x" target="_blank"></a>
    <a href="https://www.instagram.com/saini.elisabet/" class="fa fa-instagram fa-1x" target="_blank"></a>
    <a href="https://soundcloud.com/saini_elisabet" class="fa fa-soundcloud fa-1x" target="_blank"></a>
    </div>
</footer>


`
});
